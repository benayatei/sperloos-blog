# ‌Blog

 Basic and sample blog.
 
 Users can login with auth webservices and publish posts.

## Tech Stack

PHP, Laravel


## Features

- Users
- Posts

## API Reference

[PostMan Collection](./public/esperloos_blog.postman_collection.json)

## Run Locally

Clone the project

```bash
git clone git@gitlab.com:benayatei/sperloos-blog.git
```

Go to the project directory

```bash
cd sperloos-blog
```

Install dependencies

```bash
composer install
```

Run migrations

### Environment Variables

See and Complete .env file.

```bash
php artisan migrate
```

Run database seeder

```bash
php artisan db:seed
```

Run storage link

```bash
php artisan storage:link
```

And finally

```bash
php artisan serve
```


