<?php

return [
    "postNotFoundException" => "The post not found.",
    "thisActionUnauthorized" => "This action is unauthorized."
];
