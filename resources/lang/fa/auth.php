<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات وارد شده صحیح نمی‌باشد و امکان ورود وجود ندارد.',
    'password' => 'کلمه عبور صحیح نمی‌باشد.',
    'throttle' => 'تلاش برای ورود زیاد است. لطفا بعد از  :seconds ثانیه تلاش کنید.',
    "success_revoked" => "توکن شما با موفقیت منقضی شد."

];
