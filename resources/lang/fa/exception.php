<?php

return [
    "postNotFoundException" => "پست مورد نظر یافت نشد.",
    "thisActionUnauthorized" => "اجازه انجام این کار را ندارید."
];
