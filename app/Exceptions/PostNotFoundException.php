<?php

namespace App\Exceptions;

use Exception;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Throwable;

class PostNotFoundException extends NotFoundException
{
    protected string $errorCode = "POST_NOT_FOUND";

    public function __construct($code = 0, Throwable $previous = null)
    {
        parent::__construct(__("exception.postNotFoundException"), $code, $previous);
    }
}
