<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class NotFoundException extends Exception
{
    protected int $statusCode = 404;
    protected string $errorCode = "";

    public function render(): JsonResponse
    {
        return response()->json([
            "message" => $this->message,
            "errorCode" => $this->errorCode,
        ], $this->statusCode);
    }
}
