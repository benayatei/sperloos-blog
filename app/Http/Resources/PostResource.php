<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $media = $this->media->first();
        return [
            "id" => $this->id,
            "title" => $this->title,
            "content" => $this->content,
            "user" => $this->user,
            "images" => [
                "main" => $media?->getFullUrl(),
                "thumb" => $media?->getFullUrl("thumb"),
            ],
            "createdAt" => $this->created_at,
            "updatedAt" => $this->updated_at
        ];
    }
}
