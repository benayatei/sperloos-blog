<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $lang = $request->get("lang", "en");

        $lang = match ($lang) {
            "fa-ir", "fa" => "fa",
            default => "en",
        };

        App::setlocale($lang);
        return $next($request);
    }
}
