<?php

namespace App\Services;

use App\Http\Resources\AuthTokenResource;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login($email, $password, $platform_name)
    {
        $user = $this->userRepository->findByEmail($email);

        $this->attempPassword($user, $password);

        return new AuthTokenResource($user->createToken($platform_name));
    }

    private function attempPassword($user, $password)
    {
        if (!$user || !Hash::check($password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => [
                    __("auth.failed")
                ],
            ]);
        }
    }
}
