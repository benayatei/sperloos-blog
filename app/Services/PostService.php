<?php

namespace App\Services;

use App\Exceptions\PostNotFoundException;
use App\Http\Resources\PostResource;
use App\Repositories\PostRepositoryInterface;

class PostService
{
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function findAll($perPage, $order, $direction)
    {
        $data = $this->postRepository->findAll($perPage, $order, $direction);

        return PostResource::collection($data);
    }

    public function findOneById($id)
    {
        $data = $this->postRepository->findById($id);

        if (!isset($data)) {
            throw new PostNotFoundException();
        }

        return new PostResource($data);
    }

    public function store($title, $content, $image, $userId)
    {
        $data = $this->postRepository->create([
            "title" => $title,
            "content" => $content,
            "user_id" => $userId
        ]);

        $data->addMedia($image)->toMediaCollection();

        return new PostResource($data);
    }

    public function update($data, $id)
    {
        $post = $this->postRepository->findById($id);

        if (!isset($post)) {
            throw new PostNotFoundException();
        }

        $this->postRepository->update($data, $id);

        if (isset($data["image"])){
            $post->clearMediaCollection();
            $post->addMedia($data["image"])->toMediaCollection();
        }

        return $this->findOneById($id);
    }

    public function destroy($id)
    {
        $this->postRepository->destroy($id);

        return response()->json([
            "message" => __("message.success")
        ]);
    }
}
