<?php

namespace App\Repositories\Elequent;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class UserRepositoryImpl implements UserRepositoryInterface
{
    public function findByEmail($email): Model | null
    {
        return User::where('email', $email)->first();
    }
}
