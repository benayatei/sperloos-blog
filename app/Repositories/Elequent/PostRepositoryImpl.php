<?php

namespace App\Repositories\Elequent;

use App\Models\Post;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class PostRepositoryImpl implements PostRepositoryInterface
{
    private $model;

    public function __construct()
    {
        $this->model = new Post();
    }

    public function findAll($perPage, $order, $direction)
    {
        return $this->model->query()
            ->with([
                "media",
                "user" => function ($query) {
                    $query->select("id", "name", "email");
                }
            ])
            ->orderBy($order, $direction)
            ->paginate($perPage);
    }

    public function create($data): Model
    {
        return $this->model->query()->create($data);
    }

    public function findById($id): Model|null
    {
        return $this->model->query()
            ->with([
                "media",
                "user" => function ($query) {
                    $query->select("id", "name", "email");
                }
            ])
            ->find($id);
    }

    public function destroy($id): bool
    {
        return $this->model->destroy($id);
    }

    public function update($data, $id): bool
    {
        return $this->model->query()
            ->find($id)
            ->update($data);
    }
}
