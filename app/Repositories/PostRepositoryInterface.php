<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

interface PostRepositoryInterface
{
    public function findAll($perPage, $order, $direction);

    public function findById($id): Model|null;

    public function create($data): Model;

    public function destroy($id): bool;

    public function update($data, $id): bool;
}
