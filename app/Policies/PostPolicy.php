<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PostPolicy
{
    use HandlesAuthorization;


     /**
     * Determine whether the user can view any models.
     *
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     */
    public function view()
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $this->policyCondition($user->id)
            ? Response::allow()
            : Response::deny(__("exception.thisActionUnauthorized"));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user)
    {
        return $this->policyCondition($user->id)
            ? Response::allow()
            : Response::deny(__("exception.thisActionUnauthorized"));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user)
    {
        return $this->policyCondition($user->id)
            ? Response::allow()
            : Response::deny(__("exception.thisActionUnauthorized"));
    }

    private function policyCondition($userID)
    {
        return $userID % 2 == 0;
    }
}
