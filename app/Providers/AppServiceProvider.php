<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            "App\Repositories\UserRepositoryInterface",
            "App\Repositories\Elequent\UserRepositoryImpl"
        );

        $this->app->bind(
            "App\Repositories\PostRepositoryInterface",
            "App\Repositories\Elequent\PostRepositoryImpl"
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
